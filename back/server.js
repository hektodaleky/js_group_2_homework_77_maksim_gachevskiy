const express = require('express');
const cors = require('cors');
const messages = require('./messages');
const db = require('./fileDb');

const port = 8000;


const app = express();
app.use(cors());
app.use(express.json());
app.use('/', messages(db));
app.use(express.static('public'));

db.init().then(() => {
    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
});
