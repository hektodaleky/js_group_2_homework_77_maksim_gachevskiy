const express = require('express');

const multer = require('multer');
const path = require('path');

const config = require('./config');

const router = express.Router();
const nanoid = require('nanoid');

const storage = multer.diskStorage({
    destination: (req, file, cd) => {
        cd(null, config.uploadPath);
    }, filename: (req, file, cd) => {
        cd(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

const createRouter = (db) => {

    router.get('/', (req, res) => {

        res.send('HELLO !!!! :))) INPUT /MESSAGE ')
    });


    router.get('/messages', (req, res) => {
        res.send(db.getLastMessage(req.query))
    });

    router.post('/messages', upload.single('image'), (req, res) => {
        console.log(req.body);
        const messageBody = req.body;
        if (!messageBody.author || messageBody.author.length === 0) {
            messageBody.author = 'Anonymous'
        }
        if (!messageBody.message || messageBody.message.length === 0) {
            res.status(400)
                .send({error: "Message must be present in the request"});
            return;
        }

        const message = {
            id: nanoid(),
            date: new Date().toISOString(),
            author: messageBody.author,
            message: messageBody.message
        };
        if (req.file) {
            message.image = req.file.filename;
        }

        db.addMessage(message).then(() => res.send(message))

    });

    return router;
};

module.exports = createRouter;