import React from "react";
const OnePost = props => {
    return (<div className="oneBlock">
        {props.img ? <img alt="" className="main-img" style={{width: '150px', maxHeight: '150px'}}
                          src={"http://localhost:8000/uploads/" + props.img}/> : null}

        <div className="right"><p className="auth">{props.author}</p>
            <p className="post">{props.post}</p>
            <p className="time">{props.time}</p></div>
    </div>)
};
export default OnePost;