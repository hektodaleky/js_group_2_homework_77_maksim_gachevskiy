import React, {Component, Fragment} from "react";
import InputForm from "../components/InputForm/InputForm";
import OnePost from "../components/OnePost/OnePost";
import {connect} from "react-redux";
import {getData, postData} from "../store/action";
import "./Chat.css";
class Chat extends Component {
    state = {
        author: "",
        message: "",
        postsArray: [],
        image: ""
    };


    changeName = event => this.setState({author: event.target.value});

    changePost = event => this.setState({message: event.target.value});


    componentDidMount = () => {
        this.interval = setInterval(this.props.getData, 3000);

    };
    submitData = (event) => {
        event.preventDefault();
        if (!this.state.message) {
            alert("Поле message не должно быть пустым");
            return
        }
        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });
        this.props.postData(formData);
        this.setState({author: "", message: '', image: ''})
    };
    componentWillUnmount = () => {
        clearInterval(this.interval);
        console.log("Unmount");
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };


    render() {

        return (<Fragment>

                <div className="chatWindow">
                    {this.props.messages.map(post => {
                        return <OnePost author={post.author} post={post.message} time={post.date}
                                        key={post.id} img={post.image}/>
                    })}</div>
                <InputForm changeAuthor={(event => this.changeName(event))}
                           changeText={(event => this.changePost(event))}
                           clickButton={this.submitData}
                           authorValue={this.state.author} postValue={this.state.message}
                           isEmptyAuthor={this.props.authorError}
                           isEmptyText={this.props.postError}
                           change={this.fileChangeHandler}/>

            </Fragment>
        );
    }
}
;

const mapStateToProps = state => {
    return {
        messages: state.messageArray,
        postError: state.postError,
        authorError: state.authorError


    }
};

const mapDispatchToProps = dispatch => {

    return {
        postData: (data) => dispatch(postData(data)),
        getData: () => dispatch(getData())
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Chat);